'''
   Copyright (C) 2019 neo.Foletto

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
from tensorboard.compat import tf

'''
Created on Nov 11, 2019

@author: neo
'''
# !pip install keras
# !pip install tensorflow
# !pip install scikit-learn
# !pip install numpy

from keras.models import Sequential
from keras.layers import Dense
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping
import matplotlib.pyplot as plt
from src.PlotGraphic import PlotGraphicBar
import numpy

#-----------------------------------------------------------------------------
# Define custom loss functions for regression in Keras
#-----------------------------------------------------------------------------

# mean absolute percentagem error (mape) for regression
# def mean_absolute_percentage_error(y_true, y_pred):
#     from sklearn.utils import check_arrays
#     y_true, y_pred = check_arrays(y_true, y_pred)
#
#     ## Note: does not handle mix 1d representation
#     #if _is_1d(y_true):
#     #    y_true, y_pred = _check_1d_array(y_true, y_pred)
#
#     return numpy.mean(numpy.abs((y_true - y_pred) / y_true)) * 100

# root mean squared error (rmse) for regression
def rmse(y_true, y_pred):
    from keras import backend
    return backend.sqrt(backend.mean(backend.square(y_pred - y_true), axis=-1))

# mean squared error (mse) for regression
def mse(y_true, y_pred):
    from keras import backend
    return backend.mean(backend.square(y_pred - y_true), axis=-1)

# coefficient of determination (R^2) for regression
def r_square(y_true, y_pred):
    from keras import backend as K
    SS_res =  K.sum(K.square(y_true - y_pred))
    SS_tot = K.sum(K.square(y_true - K.mean(y_true)))
    return (1 - SS_res/(SS_tot + K.epsilon()))

def r_square_loss(y_true, y_pred):
    from keras import backend as K
    SS_res =  K.sum(K.square(y_true - y_pred))
    SS_tot = K.sum(K.square(y_true - K.mean(y_true)))
    return 1 - ( 1 - SS_res/(SS_tot + K.epsilon()))

# Carregando o dataset
dataset = numpy.loadtxt("../dataset/spine.csv", delimiter=",")

# Imprime o dataset
dataset

# Split em variáveis de input (X) e output (Y)
X = dataset[:, 0:12]
Y = dataset[:, 12]

# split into 67% for train and 33% for test
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33)

# https://keras.io/initializers/
# Cria o modelo
model = Sequential()
model.add(Dense(10000, input_dim=12, kernel_initializer='uniform', activation='relu'))
model.add(Dense(1000, kernel_initializer='uniform', activation='relu'))
model.add(Dense(1, kernel_initializer='uniform', activation='sigmoid'))

model.summary()

# Compilação do modelo
# Precisamos selecionar o otimizador que é o algoritmo específico usado para atualizar pesos enquanto
# treinamos nosso modelo.
# Precisamos selecionar também a função objetivo que é usada pelo otimizador para navegar no espaço de pesos
# (frequentemente, as funções objetivo são chamadas de função de perda (loss) e o processo de otimização é definido
# como um processo de minimização de perdas).
# Outras funções aqui: https://keras.io/losses/
# A função objetivo "categorical_crossentropy" é a função objetivo adequada para predições de rótulos multiclass e
# binary_crossentropy para classificação binária.
# A métrica é usada para medir a performance do modelo. Outras métricas: https://keras.io/metrics/
# As métricas são semelhantes às funções objetivo, com a única diferença de que elas não são usadas para
# treinar um modelo, mas apenas para avaliar um modelo.
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy', "mean_squared_error", rmse, r_square, 'mape'])

# Treinamento do modelo
# Epochs: Este é o número de vezes que o modelo é exposto ao conjunto de treinamento. Em cada iteração,
# o otimizador tenta ajustar os pesos para que a função objetivo seja minimizada.
# Batch_size: Esse é o número de instâncias de treinamento observadas antes que o otimizador execute uma
# atualização de peso.
earlystopping=EarlyStopping(monitor="mean_squared_error", patience=40, verbose=1, mode='auto')
result = model.fit(X_train, y_train, epochs=150, batch_size=10, validation_data=(X_test, y_test), callbacks=[earlystopping])

# plot_model(model, to_file='model_plo.png', show_shapes=True, show_layer_names=True)

# Avalia o modelo com os dados de teste
# Uma vez treinado o modelo, podemos avaliá-lo no conjunto de testes que contém novos exemplos não vistos.
# Desta forma, podemos obter o valor mínimo alcançado pela função objetivo e o melhor valor alcançado pela métrica
# de avaliação. Note-se que o conjunto de treinamento e o conjunto de teste são rigorosamente separados.
# Não vale a pena avaliar um modelo em um exemplo que já foi usado para treinamento.
# A aprendizagem é essencialmente um processo destinado a generalizar observações invisíveis e não a memorizar
# o que já é conhecido.
accuracy = model.evaluate(X_test, y_test)
print("\nLoss: %.2f, Acurácia: %.2f%%" % (accuracy[0], accuracy[1] * 100))

# Gera as previsões
y_pred = model.predict(X)

# Ajusta as previsões e imprime o resultado
rounded = [round(x[0]) for x in y_pred]
# print(rounded)
accuracy = numpy.mean(rounded == Y)
print("Acurácia das Previsões: %.2f%%" % (accuracy * 100))

# '''
# -----------------------------------------------------------------------------
# Plot learning curves including R^2 and RMSE
# -----------------------------------------------------------------------------

# plot bar graphic
PlotGraphicBar(Y, rounded, accuracy)

# plot training curve for R^2 (beware of scale, starts very low negative)
plt.plot(result.history['val_r_square'])
plt.plot(result.history['r_square'])
plt.title('model R^2')
plt.ylabel('R^2')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# plot training curve for rmse
plt.plot(result.history['rmse'])
plt.plot(result.history['val_rmse'])
plt.title('rmse')
plt.ylabel('rmse')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# print the linear regression and display datapoints
from sklearn.linear_model import LinearRegression

regressor = LinearRegression()
regressor.fit(Y.reshape(-1, 1), y_pred)
y_fit = regressor.predict(y_pred)

reg_intercept = round(regressor.intercept_[0], 4)
reg_coef = round(regressor.coef_.flatten()[0], 4)
reg_label = "y = " + str(reg_intercept) + "*x +" + str(reg_coef)

plt.scatter(Y, y_pred, color='blue', label='data')
plt.plot(y_pred, y_fit, color='red', linewidth=2, label='Linear regression\n' + reg_label)
plt.title('Linear Regression')
plt.legend()
plt.xlabel('observed')
plt.ylabel('predicted')
plt.show()

# plot training curve for mape
plt.plot(result.history['mape'])
plt.plot(result.history['val_mape'])
plt.title('mape')
plt.ylabel('mape')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# -----------------------------------------------------------------------------
# print statistical figures of merit
# -----------------------------------------------------------------------------
# '''

import sklearn.metrics, math

print("\n")
print("Mean absolute error (MAE):             %f" % sklearn.metrics.mean_absolute_error(Y, y_pred))
print("Mean squared error (MSE):              %f" % sklearn.metrics.mean_squared_error(Y, y_pred))
# print("Mean absolute percentag
# e error (MAPE): %f" % sklearn.metrics.mean_absolute_error(Y, y_pred))
print("Root mean squared error (RMSE):        %f" % math.sqrt(sklearn.metrics.mean_squared_error(Y, y_pred)))
print("R square (R^2):                        %f" % sklearn.metrics.r2_score(Y, y_pred))